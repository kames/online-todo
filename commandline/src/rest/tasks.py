import json
import logging

import requests as r

from model.task import Task
from util.keyring import *


def cycle_task(id: str) -> Task:
    server   = getServer()
    username = getUsername()
    password = getPassword()
    url = server + "/api/task/" + id + "/cycle"

    logging.debug("REST cycling task: " + url)
    result = r.post(url, "", auth = (username, password))

    if result.status_code == 401:
        print("Error authenticating, resetting login data")
        resetPassword()
        return cycle_task(id)

    return Task.from_remote_dict(json.loads(result.content))


def fetch_tasks() -> list:
    return fetch_tasks_('NOT_DONE')


def fetch_done_tasks() -> list:
    return fetch_tasks_('DONE')


# gets a list of tasks
def fetch_tasks_(mode) -> list:
    logging.debug("Trying to fetch tasks im status " + mode)

    server   = getServer()
    username = getUsername()
    password = getPassword()
    url = server + "/api/tasks?mode=" + mode

    logging.debug("REST fetch tasks from: " + url)
    result = r.get(url, auth = (username, password))

    if result.status_code == 401:
        print("Error authenticating, resetting login data")
        resetPassword()
        return fetch_tasks_(mode)

    lls: list = json.loads(result.content)
    return map(Task.from_remote_dict, lls)


def add_task(summary: str) -> Task:
    server   = getServer()
    username = getUsername()
    password = getPassword()
    url = server + "/api/task"

    logging.debug("REST adding task " + summary + ": " + url)
    result = r.post(url, summary, auth = (username, password))

    if result.status_code == 401:
        print("Error authenticating, resetting login data")
        resetPassword()
        return add_task(summary)

    return Task.from_remote_dict(json.loads(result.content))
