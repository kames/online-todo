from enum import Enum
import uuid

################################################################################
# TaskStatus
################################################################################
class TaskState(Enum):
    NEW         = 1
    IN_PROGRESS = 2
    DONE        = 3

    def cycled_value(self):
        return {
            1: TaskState.IN_PROGRESS,
            2: TaskState.DONE,
            3: TaskState.NEW
        }[self.value]

    def to_dict_value(self):
        if self.value == 1:
            return 'New'
        elif self.value == 2:
            return 'InProgress'
        else:
            return 'Done'

    def to_desc(self):
        if self.value == 1:
            return '|   |'
        elif self.value == 2:
            return '| → |'
        else:
            return '| X |'

    @classmethod
    def from_string(cls, val: str):
        if val == 'New':
            return cls.NEW
        elif val == 'InProgress':
            return cls.IN_PROGRESS
        else:
            return cls.DONE


################################################################################
# Task
################################################################################
class Task:

    def __init__(self, local_id: str, summary: str, state: TaskState):
        self.localId = local_id
        self.summary = summary
        self.state = state
        self.remoteId = ""


    def update_remote_id(self, remote_id):
        self.remoteId = remote_id


    def to_dict(self):
        return {
            'localId': self.localId,
            'remoteId': self.remoteId,
            'summary': self.summary,
            'state': self.state.to_dict_value()
        }


    def __str__(self):
        return self.state.to_desc() + ' ' + self.summary


    @classmethod
    def from_summary(cls, summary: str):
        return Task(uuid.uuid4().__str__(), summary, TaskState.NEW)


    @classmethod
    def from_dict(cls, data: dict):
        state = TaskState.from_string(data['state'])
        task = Task(data['localId'], data['summary'], state)
        task.update_remote_id(data['remoteId'])
        return task


    @classmethod
    def from_remote_dict(cls, data: dict):
        state = TaskState.from_string(data['state'])
        task = Task(uuid.uuid4().__str__(), data['summary'], state)
        task.update_remote_id(data['id'])
        return task

