import unittest

from model.task import *

class TestTask(unittest.TestCase):
    def test_task_status(self):
        self.assertEqual(TaskState.NEW.to_dict_value(), 'New')
        self.assertEqual(TaskState.NEW.to_desc(), '|   |')
        self.assertEqual(TaskState.NEW, TaskState.from_string('New'))

if __name__ == '__main__':
    unittest.main()