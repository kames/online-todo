from util.persistence import *
import keyring
import getpass

APP_NAME     = "Slow tasks"
SETTINGS_KEY = "user"



def getUsername():
    logging.debug("Getting username...")
    data = load_dict(SETTINGS_KEY)

    if 'username' in data:
        return data['username']
    else:
        username = str(input("First time, eh? Enter username: "))
        data['username'] = username
        save_dict(SETTINGS_KEY, data)
        logging.debug("Updated user info with username " + username)
        return username



def getPassword():
    logging.debug("Getting password...")
    username = getUsername()
    password = keyring.get_password(APP_NAME, username)

    if password is None:
        password = str(getpass.getpass("No password found, so... you know... enter it: "))
        keyring.set_password(APP_NAME, username, password)
        logging.debug("Updated user password")

    return password



def resetPassword():
    logging.debug("Resetting password")
    username = getUsername()
    keyring.delete_password(APP_NAME, username)
    data = load_dict(SETTINGS_KEY)
    data['username'] = None
    save_dict(APP_NAME, data)



def getServer():
    data = load_dict(SETTINGS_KEY)

    if 'server' not in data:
        server = str(input('Just tell me what to connect to: '))
        data['server'] = server
        save_dict(SETTINGS_KEY, data)
        logging.debug("Saved server to be " + server)

    return data['server']
