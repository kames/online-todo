import keyring.backends.SecretService as ss
import keyring

# set the keyring to be used
keyring.set_keyring(ss.Keyring())
