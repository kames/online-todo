from pathlib import Path

import errno
import json
import os
import logging

temp_dir = "/.slowtasks/"


def save_list(name: str, d: list):
    logging.debug("Saving list: " + name)
    with openWrite(name) as f:
        json.dump(d, f)


def save_dict(name: str, d: dict):
    logging.debug("Saving dictionary " + name)
    with openWrite(name) as f:
        json.dump(d, f)


def load_list(name: str) -> list:
    try:
        logging.debug("Loading list " + name)
        with openRead(name) as f:
            return json.load(f)
    except ValueError:
        logging.debug("Failed to load list " + name)
        return []


def load_dict(name: str) -> dict:
    try:
        logging.debug("Loading dictionary " + name)
        with openRead(name) as f:
            return json.load(f)
    except ValueError:
        logging.debug("Failed to load dictionary " + name)
        return {}



def getFilename(name):
    return str(Path.home()) + temp_dir + name + ".json"



def openWrite(name):
    filename = getFilename(name)
    if not os.path.exists(os.path.dirname(filename)):
        try:
            os.makedirs(os.path.dirname(filename))
        except OSError as exc: # Guard against race condition
            if exc.errno != errno.EEXIST:
                raise
    return open(filename, "w")



def openRead(name):
    filename = getFilename(name)
    if not os.path.exists(filename):
        raise ValueError("File $filename does not exist")
    return open(filename, "r")
