import util.persistence as p
from model.task import Task


CACHE_FILENAME = "task_cache"


def get_tasks() -> list:
    return list(map(Task.from_dict, p.load_list(CACHE_FILENAME)))


def save_tasks(tasks: list):
    p.save_list(CACHE_FILENAME, list(map(lambda t: t.to_dict(), tasks)))
