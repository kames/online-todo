import enum
import logging

import util.persistence as persistence
import service.task_cache as cache
import rest.tasks as rest


QUEUE_DICT_NAME = "operations_queue"


class OperationType(enum.Enum):
    ADD   = 1
    CYCLE = 2


class Operation:

    def __init__(self, local_id: str, summary: str, type: OperationType):
        self.local_id = local_id
        self.summary = summary
        self.type = type


    def to_dict(self):
        return {
            'local_id': self.local_id,
            'summary': self.summary,
            'type': self.type.value
        }


    def __str__(self):
        return self.type.name + ": " + self.local_id


    @classmethod
    def from_dict(cls, data: dict):
        local_id = data['local_id']
        summary = data['summary']
        type = OperationType(data['type'])
        return Operation(local_id, summary, type)


    def execute(self):
        if self.type == OperationType.ADD:
            logging.debug("Executing ADD operation")
            remote_task = rest.add_task(self.summary)
            tasks = cache.get_tasks()
            local_task = next(x for x in tasks if x.localId == self.local_id)
            local_task.remoteId = remote_task.remoteId
            cache.save_tasks(tasks)

        elif self.type == OperationType.CYCLE:
            logging.debug("Executing CYCLE operation")
            tasks = cache.get_tasks()
            local_task = next(x for x in tasks if x.localId == self.local_id)
            remote_task = rest.cycle_task(local_task.remoteId)
            local_task.state = remote_task.state
            cache.save_tasks(tasks)



def add_queue_item(item: Operation):
    logging.debug("Adding item to queue: " + item.__str__())
    operations = persistence.load_list(QUEUE_DICT_NAME)
    operations.append(item.to_dict())
    persistence.save_list(QUEUE_DICT_NAME, operations)


def flush_queue_items():
    logging.debug("Trying to flush operations to server")
    operations = persistence.load_list(QUEUE_DICT_NAME)

    try:
        for o in operations.copy():
            op = Operation.from_dict(o)
            op.execute()
            operations.remove(o)
    finally:
        persistence.save_list(QUEUE_DICT_NAME, operations)

