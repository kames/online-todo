from model.task import *
from service.task_operation_queue import OperationType, Operation

import logging
import service.task_operation_queue as queue
import service.task_cache           as cache
import rest.tasks                   as rest


def cycle_task(task) -> Task:
    logging.debug("Cycling task: " + task.__str__())

    # cycle task
    task.state = task.state.cycled_value()

    # save in local cache
    tasks = cache.get_tasks()
    cached_task = next(x for x in tasks if x.localId == task.localId)
    cached_task.state = task.state
    cache.save_tasks(tasks)

    # add queue operation
    queue.add_queue_item(Operation(task.localId, "", OperationType.CYCLE))

    # try to flush operations
    try:
        queue.flush_queue_items()
    except:
        print("Failed to flush operations to server. Adding to action queue...")

    return task


def add_task(summary) -> Task:
    logging.debug("Adding task with summary: " + summary)

    # create task
    task = Task.from_summary(summary)

    # save in local cahce
    tasks = cache.get_tasks()
    tasks.insert(0, task)
    cache.save_tasks(tasks)

    # add queue operation
    queue.add_queue_item(Operation(task.localId, task.summary, OperationType.ADD))

    # try to flush operation
    try:
        queue.flush_queue_items()
    except:
        print("Failed to flush operations to server. Adding to action queue...")

    return task


def get_tasks() -> list:
    logging.debug("Getting tasks")
    update_local_cache()
    return cache.get_tasks()


def get_local_tasks() -> list:
    logging.debug("Getting local tasks")
    return cache.get_tasks()


def update_local_cache():
    try:
        logging.debug("Updating local cache")
        queue.flush_queue_items()
        cache.save_tasks(rest.fetch_tasks())
    except Exception as e:
        logging.debug(e)
        print("Failed to fetch tasks from the server. Using local cache.")
