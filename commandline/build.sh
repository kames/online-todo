#!/usr/bin/env bash

# clear and create build dir
rm -rf build
rm *.spec

mkdir build

# build python exec
pyinstaller --onefile --distpath build/deb/usr/local/bin src/stl.py
pyinstaller --onefile --distpath build/deb/usr/local/bin src/sta.py
pyinstaller --onefile --distpath build/deb/usr/local/bin src/sta.py
pyinstaller --onefile --distpath build/deb/usr/local/bin src/stld.py
pyinstaller --onefile --distpath build/deb/usr/local/bin src/stcd.py

# create deb file
mkdir build/deb/DEBIAN
cp stuff/control build/deb/DEBIAN
cd build
dpkg-deb --build deb slowtasks_1.0-1.deb
