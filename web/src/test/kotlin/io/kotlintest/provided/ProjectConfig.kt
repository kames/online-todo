package io.kotlintest.provided

////////////////////////////////////////////////////////////////////////////////
// @Copyright Magic Turtle Software, 2019                                     //
////////////////////////////////////////////////////////////////////////////////
import io.kotlintest.AbstractProjectConfig
import io.kotlintest.extensions.ProjectLevelExtension
import io.kotlintest.spring.SpringAutowireConstructorExtension



class ProjectConfig : AbstractProjectConfig() {
    override fun extensions(): List<ProjectLevelExtension> = listOf(SpringAutowireConstructorExtension)
}