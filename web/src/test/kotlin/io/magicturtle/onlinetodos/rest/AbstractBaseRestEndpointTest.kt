////////////////////////////////////////////////////////////////////////////////
// @Copyright Magic Turtle Software, 2019
////////////////////////////////////////////////////////////////////////////////
package io.magicturtle.onlinetodos.rest



import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import io.kotlintest.specs.WordSpec
import io.magicturtle.onlinetodos.BackendApplication
import io.magicturtle.onlinetodos.service.UserService
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.test.web.client.getForEntity
import org.springframework.boot.test.web.client.postForEntity
import org.springframework.http.*
import org.springframework.test.context.TestPropertySource


/**
 * @author Marek Broda (@Magic Turtle Software)
 */
@SpringBootTest(
    webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT,
    classes = [BackendApplication::class],
    properties = [ "classpath:application-test.properties" ])
@TestPropertySource(locations = ["classpath:application-test.properties"])
abstract class AbstractBaseRestEndpointTest : WordSpec() {

    val mapper = ObjectMapper().registerKotlinModule()



    fun <T> get(host: String, clazz: Class<T>): Pair<HttpStatus, T?> {
        val result: ResponseEntity<String> = REST.getForEntity(host, String::class)
        return parseResult(result, clazz)
    }



    fun <T> post(host: String, content: String, clazz: Class<T>): Pair<HttpStatus, T?> {
        val headers = HttpHeaders()
        headers.contentType = MediaType.APPLICATION_JSON

        val entity = HttpEntity(content, headers)
        val result: ResponseEntity<String> = REST.postForEntity(host, entity, String::class.java)
        return parseResult(result, clazz)
    }



    private fun <T> parseResult(result: ResponseEntity<String>, clazz: Class<T>): Pair<HttpStatus, T?> {
        if (result.statusCode != HttpStatus.OK) {
            return Pair(result.statusCode, null)
        } else {
            return Pair(result.statusCode, mapper.readValue(result.body, clazz))
        }
    }


    companion object {
        const val TEST_USERNAME = "An Cient"
        const val TEST_EMAIL = "ancient@netscape.net"
        const val TEST_PASSWORD = "admin123"
        const val TEST_SERVER = "http://localhost:8086"
        val REST = TestRestTemplate(UserService.DEFAULT_USER_EMAIL, TEST_PASSWORD)
    }
}