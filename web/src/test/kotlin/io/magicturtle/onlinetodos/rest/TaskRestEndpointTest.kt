////////////////////////////////////////////////////////////////////////////////
// Copyright @ Magic Turtle Software, 2019                                     /
////////////////////////////////////////////////////////////////////////////////
package io.magicturtle.onlinetodos.rest

import io.kotlintest.matchers.collections.shouldHaveSize
import io.kotlintest.matchers.date.shouldBeAfter
import io.kotlintest.shouldBe
import io.magicturtle.onlinetodos.model.Comment
import io.magicturtle.onlinetodos.model.Task
import io.magicturtle.onlinetodos.model.TaskStatus
import io.magicturtle.onlinetodos.service.TaskService
import io.magicturtle.onlinetodos.service.UserService
import org.springframework.http.HttpStatus
import org.springframework.http.HttpStatus.*
import java.time.LocalDateTime


/**
 * @author Marek Broda (@Magic Turtle Software)
 */
class TaskRestEndpointTest( val taskService: TaskService
                          , val userService: UserService
                          ) : AbstractBaseRestEndpointTest() {

    lateinit var task1: Task
    lateinit var task2: Task
    lateinit var task3: Task
    lateinit var task4: Task
    lateinit var task5: Task

    init {
        initTestData()

        "getAllTodoItems" should {
            "return no error" {
                getAllTasks().first shouldBe OK
            }

            "return 3 items" {
                getAllTasks().second?.shouldHaveSize(3)
            }

            "fill task data correctly" {
                val tasks = getAllTasks().second!!
                val task = tasks.stream()
                                .filter { it["id"] == task1.id }
                                .findAny()
                task.isPresent shouldBe true
                task.get()["state"]   shouldBe "New"
                task.get()["summary"] shouldBe "Task 1"
            }

            "sort items according to creation date" {
                val tasks = getAllTasks().second!!
                for (i in 1 until tasks.size-1) {
                    val newTask = LocalDateTime.parse(tasks[i - 1]["creationDate"])
                    val oldTask = LocalDateTime.parse(tasks[i]["creationDate"])
                    newTask shouldBeAfter oldTask
                }
            }

            "return 400 on bad mode value" {
                getAllTasks("WRONG_MODE").first shouldBe BAD_REQUEST
            }

            "return only done task with 'done' mode" {
                val result = getAllTasks("DONE")
                result.first shouldBe OK
                result.second!!.forEach { it["state"] shouldBe "Done" }
            }
        }

        "getTask" should {
            "return 404 on non existent task" {
                getTask("ABC").first shouldBe NOT_FOUND
            }

            "return 404 on task belonging to someone else" {
                getTask(task5.id).first shouldBe NOT_FOUND
            }

            "return all attributes correctly" {
                val task = getTask(task1.id).second!!
                task["id"]          shouldBe task1.id
                task["state"]       shouldBe task1.state.name
                task["summary"]     shouldBe task1.summary
                task["details"]     shouldBe task1.details
                task["comments"]    shouldBe emptyList<Comment>()
            }
        }

        "createTask" should {
            "create task with correct summary" {
                val task = createTask("ABC")

                task.first shouldBe OK
                task.second!!["summary"] shouldBe "ABC"
            }

            "not create a task with empty summary" {
                createTask("").first shouldBe BAD_REQUEST
            }

            "not create a task with too long summary" {
                fun doubler(text: String, times: Int): String {
                    if (times < 1) return text else return doubler(text + text, times - 1) }

                createTask(doubler("A", 10)).first shouldBe BAD_REQUEST
            }
        }

        "updateTask" should {
            "return 404 on non existent task" {
                updateTask("ABC", "1", "2", "New").first shouldBe NOT_FOUND
            }

            "return 404 on someone else's task" {
                updateTask(task5.id, "1", "2", "New").first shouldBe NOT_FOUND
            }

            "return 400 for invalid parameters (summary, description, state)" {
                updateTask(task1.id, "", "", "").first shouldBe BAD_REQUEST
            }

            "update a task correctly" {
                val r = updateTask(task1.id, "SUMM", "DETT", "Done").second!!
                r["summary"] shouldBe "SUMM"
                r["details"] shouldBe "DETT"
                r["state"] shouldBe "Done"
            }
        }

        "cycleTask" should {
            "return 404 for non existent task" {
                cycleTask("ABC").first shouldBe NOT_FOUND
            }

            "return 404 for someone elses task" {
                cycleTask(task5.id).first shouldBe NOT_FOUND
            }

            "cycle correctly a new task" {
                cycleTask(task1.id).second!!["state"] shouldBe TaskStatus.InProgress.name
            }

            "cycle correctly an in progress task" {
                cycleTask(task2.id).second!!["state"] shouldBe TaskStatus.Done.name
            }

            "cycle correctly a done task" {
                cycleTask(task3.id).second!!["state"] shouldBe TaskStatus.New.name
            }
        }

        "addComment" should {
            "return 404 for non-existent task" {
                addComment("ABC", "XXXX").first shouldBe NOT_FOUND
            }

            "return 404 for someone else's task" {
                addComment(task5.id, "XXXX").first shouldBe NOT_FOUND
            }

            "add a comment at the end" {
                val comment = addComment(task1.id, "This is a comment").second!!

                comment["content"] shouldBe "This is a comment"
                comment["taskId"]  shouldBe task1.id

                val task = getTask(task1.id).second!!

                (task["comments"] as List<HashMap<String,String>>).size       shouldBe 1
                (task["comments"] as List<HashMap<String,String>>)[0]["content"] shouldBe "This is a comment"
            }
        }
    }



    private fun initTestData() {
        var user = userService.getOrCreateDefaultUser()
        user = userService.updatePassword(TEST_PASSWORD, user)

        var anotherUser = userService.getOrCreateUser(TEST_EMAIL, TEST_USERNAME)
        anotherUser = userService.updatePassword(TEST_PASSWORD, anotherUser)

        with(taskService) {
            // normal task
            task1 = createTask("Task 1", user).force()
            // in progress task
            task2 = cycleTask(createTask("Task 2", user).force().id, user)
            // done task
            task3 = cycleTask(cycleTask(createTask("Task 3", user).force().id, user).id, user)
            // normal task
            task4 = createTask("Task 4", user).force()
            // task from another user
            task5 = createTask("Task 5", anotherUser).force()
        }
    }



    fun getAllTasks(): Pair<HttpStatus, List<HashMap<String, String>>?> {
        return get("$TEST_SERVER/api/tasks/", List::class.java)
            as Pair<HttpStatus, List<HashMap<String, String>>?>
    }



    fun getAllTasks(mode: String): Pair<HttpStatus, List<HashMap<String, String>>?> {
        return get("$TEST_SERVER/api/tasks?mode=$mode", List::class.java)
            as Pair<HttpStatus, List<HashMap<String, String>>?>
    }



    fun getTask(id: String): Pair<HttpStatus, HashMap<String,Object>?> {
        return get("$TEST_SERVER/api/task/$id", HashMap::class.java)
            as Pair<HttpStatus, HashMap<String,Object>?>
    }



    fun cycleTask(id: String): Pair<HttpStatus, HashMap<String,String>?> {
        return post("$TEST_SERVER/api/task/$id/cycle", "", HashMap::class.java)
            as Pair<HttpStatus, HashMap<String,String>?>
    }



    fun createTask(summary: String): Pair<HttpStatus, HashMap<String,String>?> {
        return post("$TEST_SERVER/api/task/", summary, HashMap::class.java)
            as Pair<HttpStatus, HashMap<String,String>?>
    }



    fun updateTask(id: String, summary: String, details: String, status: String): Pair<HttpStatus, HashMap<String,String>?> {
        val t = """{ "id": "$id", "summary": "$summary", "details": "$details", "state": "$status", "comments": [] }"""
        return post("$TEST_SERVER/api/task/$id", t, HashMap::class.java)
            as Pair<HttpStatus, HashMap<String,String>?>
    }



    fun addComment(taskId: String, comment: String): Pair<HttpStatus, HashMap<String, String>?> {
        return post("$TEST_SERVER/api/task/$taskId/comment", comment, HashMap::class.java)
            as Pair<HttpStatus, HashMap<String, String>?>
    }
}
