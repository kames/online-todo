module Rest.Users exposing (updatePassword)

import Http exposing (..)


updatePassword: String -> (Result Http.Error String -> a) -> Cmd a
updatePassword newPassword f =
    Http.post { url = "/api/settings/updatePassword"
              , body = stringBody "application/text" newPassword
              , expect = Http.expectString f
    }
