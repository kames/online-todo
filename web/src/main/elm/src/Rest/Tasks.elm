module Rest.Tasks exposing (fetchTasks, addTask, getTaskDetails, updateTaskDetails)

import Http exposing (..)
import Json.Decode as D

import Tasks.Task exposing (..)
import Tasks.Details.Model exposing (TaskDetails, encode)



fetchTasks: (Result Http.Error (List Task) -> a) -> Cmd a
fetchTasks f =
    Http.get { url = "/api/tasks"
             , expect = Http.expectJson f tasksDecoder
    }



addTask: String -> (Result Http.Error Task -> a) -> Cmd a
addTask task f =
    Http.post { url = "/api/task"
              , body = stringBody "application/text" task
              , expect = Http.expectJson f taskDecoder
    }



getTaskDetails: String -> (Result Http.Error TaskDetails -> a) -> Cmd a
getTaskDetails id f =
    Http.get { url = "/api/task/" ++ id
             , expect = Http.expectJson f taskDetailsDecoder}



updateTaskDetails: TaskDetails -> (Result Http.Error () -> a) -> Cmd a
updateTaskDetails taskDetails f =
    Http.post { url = "/api/task/" ++ taskDetails.id
              , expect = Http.expectWhatever f
              , body = Http.jsonBody <| encode taskDetails }



--------------------------------------------------------------------------------
-- JSON decoders
--------------------------------------------------------------------------------
taskStatusDecoder: D.Decoder TaskStatus
taskStatusDecoder =
    D.string |> D.andThen (
        \str ->
            case str of
                "New"        -> D.succeed New
                "InProgress" -> D.succeed InProgress
                "Done"       -> D.succeed Done
                unknown      -> D.fail <| "Unknown task status " ++ unknown
    )


taskDecoder: D.Decoder Task
taskDecoder =
    D.map3 Task
        (D.field "id" D.string)
        (D.field "summary" D.string)
        (D.field "state" taskStatusDecoder)


tasksDecoder: D.Decoder (List Task)
tasksDecoder = D.list taskDecoder



taskDetailsDecoder: D.Decoder TaskDetails
taskDetailsDecoder =
    D.map4 TaskDetails
        (D.field "id" D.string)
        (D.field "summary" D.string)
        (D.field "details" D.string)
        (D.field "state" taskStatusDecoder)
