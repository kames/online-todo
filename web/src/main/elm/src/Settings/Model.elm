module Settings.Model exposing (..)



type alias Model =
    { newPassword:     String
    , confirmPassword: String }



initialModel: Model
initialModel = Model "" ""


setNewPassword: Model -> String -> Model
setNewPassword model pass =
    { model | newPassword = pass }

setConfirmPassword: Model -> String -> Model
setConfirmPassword model pass =
    { model | confirmPassword = pass }

resetPassword: Model -> Model
resetPassword model =
    { model | newPassword = ""
            , confirmPassword = "" }
