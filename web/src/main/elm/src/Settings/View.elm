module Settings.View exposing (view)

import Html exposing (Html, div, text, input)
import Html.Attributes exposing (class, type_, value)
import Html.Events exposing (onInput)

import Settings.Model  exposing (..)
import Settings.Action exposing (..)

import Common.Button exposing (dosButton)



view: Model -> Html SettingsAction
view model =
    div [ class "settings-area" ]
        [ div [ ] [
            div []
                [ text "New password:     "
                , input [ type_ "password", class "new-task-input", value model.newPassword, onInput UpdateNewPassword ] [] ]
            , div []
                [ text "Confirm password: "
                , input [ type_ "password", class "new-task-input", value model.confirmPassword, onInput UpdateConfirmPassword ] [] ]
            , div [] []
            , dosButton "Update password" ChangePassword <| (model.newPassword /= model.confirmPassword) || (String.length model.newPassword == 0)
            ]
        , div [ ]
            [ div [ ]
                [ text "Linux app - deb file: "
                , text "<<THIS SHOULD BE THE LINK>>"
                ]
            , div [ ]
                [ text "Android app:          "
                , text "<<THIS SHOULD BE THE LINK>>"
                ]
            ]
        ]