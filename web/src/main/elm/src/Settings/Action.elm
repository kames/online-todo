module Settings.Action exposing (..)

import Settings.Model exposing (..)
import Rest.Users as RU
import Debug exposing (toString)

import Common.Actions as CA

import Http

type SettingsAction
    = ChangePassword
    | PasswordChanged (Result Http.Error String)
    | UpdateNewPassword String
    | UpdateConfirmPassword String


update: SettingsAction -> Model -> (Model, Cmd SettingsAction, CA.ActionResult)
update action model =
    case action of
        UpdateNewPassword newPass ->
            ( setNewPassword model newPass, Cmd.none, CA.Success )
        UpdateConfirmPassword confPass ->
            ( setConfirmPassword model confPass, Cmd.none, CA.Success )
        ChangePassword ->
            ( resetPassword model, RU.updatePassword model.newPassword PasswordChanged, CA.Success)
        PasswordChanged result ->
            handlePasswordChangedResult result model



handlePasswordChangedResult: (Result Http.Error String) -> Model -> (Model, Cmd SettingsAction, CA.ActionResult)
handlePasswordChangedResult result model =
    case result of
        Ok _  -> (model, Cmd.none, CA.SuccessWithMessage "Password updated")
        Err e -> (model, Cmd.none, CA.NetworkError ("Error changing password: " ++ (toString e)))