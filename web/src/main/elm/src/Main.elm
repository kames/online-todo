module Main exposing (..)

import Time
import Browser
import Html exposing (Html, text, Attribute, div)
import Html.Attributes exposing (class, style)

import Model exposing (..)
import Action exposing (..)
import Sidebar as S

import Tasks.View as TV
import Notes.Notes as NN
import Settings.View as SV

import Common.BorderBox exposing (..)



main = Browser.element { init = init, view = view, update = update, subscriptions = subscriptions }



init : () -> (Model, Cmd Action)
init _ = ( initialModel, initialAction )

subscriptions : Model -> Sub Action
subscriptions model =
    if model.message == Nothing then
        Sub.none
    else
        Time.every 3000 <| always HideMessage



--------------------------------------------------------------------------------
-- VIEW
--------------------------------------------------------------------------------
view : Model -> Html Action
view model =
    div [ class "top-div" ] [
        div [ ] [ messageView model.message ],
        div [ ] [ text " " ],
        div [ ] [ text "Console ToDo" ],
        div [ class "below-title"] [
            div [ class "sidebar" ] [ S.sidebarHtml model ],
            div [ class "content" ] [ moduleView model ]
        ]
    ]



messageView: Maybe MessageBanner -> Html Action
messageView banner =
    case banner of
        Nothing   -> div [] [ text " " ]
        Just msg  -> div [ style "background-color" (if msg.msgType == Error then "red" else "green")]
                         [ text msg.context ]



moduleView: Model -> Html Action
moduleView model =
        case model.mode of
            ModeNotes    -> NN.notesHtml model.notesModel
            ModeSettings ->
                case model.settingsModel of
                    Nothing -> div [] []
                    Just sm -> borderBox "Settings" <| Html.map ForSettings <| SV.view sm
            ModeTasks    -> borderBox "Tasks" <| Html.map ForTask <| TV.view model.tasksModel

