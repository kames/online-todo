module Sidebar exposing (..)

import Common.BorderBox exposing (..)
import Model exposing (..)
import Action exposing (..)
import Html  exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)


sidebarHtml: Model -> Html Action
sidebarHtml _ =
    borderBox "Menu"
        <| div [ ]
               [ div [ class "clickable", onClick SwitchToTasks    ] [ text "Tasks"    ]
               , div [ class "clickable", onClick SwitchToNotes    ] [ text "Notes"    ]
               , div [ class "clickable", onClick SwitchToSettings ] [ text "Settings" ]
           ]
