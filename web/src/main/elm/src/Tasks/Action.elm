module Tasks.Action exposing (..)

import Http
import Debug exposing (toString)

import Common.Actions as CA
import Tasks.Task     exposing (..)
import Tasks.Model    exposing (..)
import Rest.Tasks     exposing (..)
import Tasks.Details.Action as TDA



--------------------------------------------------------------------------------
-- Action
--------------------------------------------------------------------------------
type Action
    = TasksLoaded (Result Http.Error (List Task))
    | AddTask
    | TaskAdded (Result Http.Error Task)
    | UpdateNewTask String
    | ReloadTasks
    | ShowTask String
    | DetailsAction TDA.TaskDetailsAction
    | ToggleShowDoneTasks
    | DoAbsolutelyNothing



--------------------------------------------------------------------------------
-- update function
--------------------------------------------------------------------------------
update : Action -> Model -> (Model, Cmd Action, CA.ActionResult)
update action model =
        case action of
            TasksLoaded result    -> handleTasksLoaded result model
            AddTask               -> handleAddTask model
            TaskAdded result      -> handleTaskAdded result model
            UpdateNewTask summary -> handleUpdateNewTask model summary
            ReloadTasks           -> handleReloadTasks model
            DetailsAction acn     -> handleDetailsAction acn model
            ShowTask taskId       -> handleShowTask taskId model
            ToggleShowDoneTasks   -> handleToggleShowDoneTasks model
            DoAbsolutelyNothing   -> ( model, Cmd.none, CA.Success )



--------------------------------------------------------------------------------
-- initial action
--------------------------------------------------------------------------------
initialAction: Cmd Action
initialAction = fetchTasks TasksLoaded



--------------------------------------------------------------------------------
-- handlers
--------------------------------------------------------------------------------
handleTasksLoaded: Result Http.Error (List Task) -> Model -> (Model, Cmd Action, CA.ActionResult)
handleTasksLoaded result model =
    let
        newModel = { model | tasksLoading = False }
    in
        case result of
            Ok tasks   -> ( { newModel | tasks = tasks }, Cmd.none, CA.Success )
            Err reason -> ( { newModel | tasks = []    }, Cmd.none, CA.NetworkError ("Error fetching tasks from server: " ++ (toString reason) ))



handleAddTask: Model -> (Model, Cmd Action, CA.ActionResult)
handleAddTask model =
    let
        newModel = { model | newTaskAdding = True }
        newCmd   = addTask model.newTaskSummary TaskAdded
    in
        ( newModel, newCmd, CA.Success )



handleTaskAdded: Result Http.Error Task -> Model -> (Model, Cmd Action, CA.ActionResult)
handleTaskAdded result model =
    case result of
        Ok task ->
            let
                newModel = { model | tasks = task :: model.tasks
                                   , newTaskSummary = ""
                                   , newTaskAdding = False }
            in
                ( newModel, Cmd.none, CA.SuccessWithMessage <| "Added task " ++ task.summary )
        Err reason ->
            let
                newModel = { model | newTaskSummary = ""
                                   , newTaskAdding = False }
            in
                ( newModel, Cmd.none, CA.NetworkError ("Error adding task: " ++ (toString reason)) )



handleUpdateNewTask: Model -> String -> (Model, Cmd Action, CA.ActionResult)
handleUpdateNewTask model summary =
    let
        newModel = { model | newTaskSummary = summary }
    in
        ( newModel, Cmd.none, CA.Success )



handleReloadTasks: Model -> (Model, Cmd Action, CA.ActionResult)
handleReloadTasks model =
    let
        newModel = { model | tasksLoading = True }
    in
        ( newModel, fetchTasks TasksLoaded, CA.Success )



handleDetailsAction: TDA.TaskDetailsAction -> Model -> (Model, Cmd Action, CA.ActionResult)
handleDetailsAction a m =
    let
        ( dModel, dAction, dResult ) = TDA.update a m.details
        detailsAction = Cmd.map DetailsAction dAction
        actions = case dResult of
            CA.SuccessWithMessage _ -> Cmd.batch [ CA.toCmd ReloadTasks, detailsAction ]
            _                       -> detailsAction
    in
        ( { m | details = dModel }, actions, dResult )



handleShowTask: String -> Model -> (Model, Cmd Action, CA.ActionResult)
handleShowTask id model =
    ( model, Cmd.map DetailsAction <| TDA.initialAction id, CA.Success)



handleToggleShowDoneTasks: Model -> (Model, Cmd Action, CA.ActionResult)
handleToggleShowDoneTasks model =
    ( { model | showDone = not model.showDone }, Cmd.none, CA.Success )
