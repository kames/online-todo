module Tasks.Module exposing (..)

import Tasks.Model exposing (..)
import Tasks.Action exposing (..)
import Tasks.View exposing (..)

import Common.Actions as CA
import Common.Module  as CM


tasksModule: CM.Module Action Model
taskModule = CM.Module view update ( initialModel, Cmd.none, CA.Success )