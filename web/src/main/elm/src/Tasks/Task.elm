module Tasks.Task exposing (..)



-- TaskStatus
type TaskStatus
    = New
    | InProgress
    | Done



isTaskFinished: Task -> Bool
isTaskFinished t =
    case t.state of
        Done -> True
        _    -> False



taskStatusFromString : String -> TaskStatus
taskStatusFromString name =
    case name of
        "New"        -> New
        "InProgress" -> InProgress
        _            -> Done



toTaskStatusLabel: TaskStatus -> String
toTaskStatusLabel s =
    case s of
        New        -> "|   |"
        InProgress -> "| → |"
        Done       -> "| X |"



toTaskStatusName: TaskStatus -> String
toTaskStatusName s =
    case s of
        New        -> "New"
        InProgress -> "InProgress"
        Done       -> "Done"



toTaskStatusDescription: TaskStatus -> String
toTaskStatusDescription s =
    case s of
        New        -> "new"
        InProgress -> "in progress"
        Done       -> "done"



-- Task
type alias Task =
    { id: String
    , summary: String
    , state: TaskStatus }



taskToString: Task -> String
taskToString task = (toTaskStatusLabel task.state) ++ " " ++ task.summary



tasksToString: List Task -> String
tasksToString tasks =
    case tasks of
        t::ts -> (taskToString t) ++ "\n" ++ (tasksToString ts)
        []    -> ""
