module Tasks.View exposing (..)

import Common.BorderBox exposing (..)
import Common.Button exposing (..)
import Html exposing (Html, Attribute, div, text, input)
import Html.Attributes exposing (value, class, style)
import Html.Events exposing (on, onInput, onClick)
import Keyboard.Event exposing (..)
import Keyboard.Key exposing (Key(..))
import Json.Decode as Json

import Tasks.Model  exposing (..)
import Tasks.Task   exposing (..)
import Tasks.Action exposing (..)
import Tasks.Details.Model as DM
import Tasks.Details.View as DV



view: Model -> Html Action
view model =
        div [ class "task-area" ] [
            div [ class "task-list" ] [
                if model.showDone then
                    taskList <| finishedTasks <| model.tasks
                else
                    div [] [
                        div []
                            [ text "  >   "
                            , input [ value model.newTaskSummary
                                    , onInput UpdateNewTask
                                    , onKeyPress mapKeyToAction
                                    , class "new-task-input" ] []
                            ]
                        , taskList <| unfinishedTasks <| model.tasks
                        ],
                div [] [],
                toggleButton model
            ],
            div [ class "task-details" ] [
                taskDetails model.details
            ]
        ]



taskDetails: DM.Model -> Html Action
taskDetails m =
    case m of
        Nothing -> div [] []
        Just _ -> Html.map DetailsAction (DV.view m)



taskList: List Task -> Html Action
taskList ts = div [ ] ( ts |> List.map toDiv)



toDiv: Task -> Html Action
toDiv task =
    let
        taskString = taskToString task
        event = ShowTask task.id
    in
        div [ onClick event, class "clickable" ] [ text taskString ]



mapKeyToAction: KeyboardEvent -> Action
mapKeyToAction k =
    case k.keyCode of
        Enter -> AddTask
        _     -> DoAbsolutelyNothing



onKeyPress : (KeyboardEvent -> a) -> Attribute a
onKeyPress actionMapper = on "keydown" <| Json.map actionMapper decodeKeyboardEvent



finishedTasks: List Task -> List Task
finishedTasks ts =
    List.filter isTaskFinished ts

unfinishedTasks: List Task -> List Task
unfinishedTasks ts =
    List.filter (\t -> not <| isTaskFinished t) ts



toggleButton: Model -> Html Action
toggleButton m =
    dosButton (if m.showDone then "Hide done" else "Show done") ToggleShowDoneTasks False