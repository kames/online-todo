module Tasks.Details.View exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)

import Common.BorderBox as BB
import Common.Button as CB
import Tasks.Details.Model  exposing (..)
import Tasks.Details.Action exposing (..)
import Tasks.Task exposing (..)


view: Model -> Html TaskDetailsAction
view m =
    case m of
        Nothing ->
            div [] [ text "Loading..." ]
        Just td ->
            BB.borderBox td.taskDetails.summary <|
                div [ class "task-details-container" ] [
                    div []
                        [ text "Summary: "
                        , input [ class "new-task-input", value td.taskDetails.summary, onInput SyncSummary, disabled td.updating ] [] ],
                    div []
                        [ text "Status:  "
                        , CB.dosButton (toTaskStatusDescription td.taskDetails.state) ToggleState td.updating ],
                    div []
                        [ text "Details: " ],
                    div []
                        [ textarea [ class "input-multiline", value td.taskDetails.details, onInput SyncDetails, disabled td.updating ] [] ],
                    div []
                        [ CB.dosButton "Close" CloseDetails False, text " ", CB.dosButton "Update" UpdateTask (not td.dirty) ]
                ]
