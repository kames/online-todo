module Tasks.Details.Model exposing (..)

import Json.Encode exposing (..)
import Tasks.Task exposing (..)



type alias Model = Maybe TaskDetailsData



type alias TaskDetailsData =
    { taskDetails: TaskDetails
    , dirty: Bool
    , updating: Bool }



type alias TaskDetails =
    { id: String
    , summary: String
    , details: String
    , state: TaskStatus
    }



initialModel: Model
initialModel = Nothing



encode: TaskDetails -> Value
encode td =
    object
        [ ( "id", string td.id )
        , ( "summary", string td.summary )
        , ( "details", string td.details )
        , ( "state", string <| toTaskStatusName td.state )
        ]