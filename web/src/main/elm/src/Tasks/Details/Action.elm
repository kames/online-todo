module Tasks.Details.Action exposing (..)

import Http
import Tasks.Task exposing (TaskStatus(..))
import Tasks.Details.Model exposing (TaskDetails, Model, TaskDetailsData)
import Common.Actions as CA
import Rest.Tasks as TR
import Debug exposing (toString)



--------------------------------------------------------------------------------
-- action
--------------------------------------------------------------------------------
type TaskDetailsAction = CloseDetails
                       | SyncSummary String
                       | SyncDetails String
                       | ToggleState
                       | UpdateTask
                       | TaskUpdated (Result Http.Error ())
                       | TaskLoaded (Result Http.Error TaskDetails)



--------------------------------------------------------------------------------
-- update function
--------------------------------------------------------------------------------
update: TaskDetailsAction -> Model -> (Model, Cmd TaskDetailsAction, CA.ActionResult)
update action model =
    case model of
        Nothing -> case action of
            TaskLoaded result -> handleTaskLoaded result model
            _ -> ( Nothing, Cmd.none, CA.Success )

        Just taskDetailsData -> case action of
            TaskLoaded result      -> handleTaskLoaded result model
            SyncSummary newSummary -> handleSyncSummary newSummary taskDetailsData
            SyncDetails newDetails -> handleSyncDetails newDetails taskDetailsData
            ToggleState            -> handleToggleState taskDetailsData
            UpdateTask             -> handleTaskUpdating taskDetailsData
            TaskUpdated result     -> handleTaskUpdated result taskDetailsData
            CloseDetails           -> ( Nothing, Cmd.none, CA.Success )



--------------------------------------------------------------------------------
-- initial action
--------------------------------------------------------------------------------
initialAction: String -> Cmd TaskDetailsAction
initialAction id =
    TR.getTaskDetails id TaskLoaded



--------------------------------------------------------------------------------
-- handlers
--------------------------------------------------------------------------------
handleTaskLoaded: (Result Http.Error TaskDetails) -> Model -> (Model, Cmd TaskDetailsAction, CA.ActionResult)
handleTaskLoaded result _ =
    case result of
        Ok td -> ( Just (TaskDetailsData td False False), Cmd.none, CA.Success )
        Err e -> ( Nothing, Cmd.none, CA.NetworkError ("Error loading task details: " ++ (toString e)))



handleSyncSummary: String -> TaskDetailsData -> (Model, Cmd TaskDetailsAction, CA.ActionResult)
handleSyncSummary newSummary data =
    let
        taskDetails = data.taskDetails
        newTaskDetails = { taskDetails | summary = newSummary }
        newModel = Just { data | dirty = True, taskDetails = newTaskDetails }
    in
       ( newModel, Cmd.none, CA.Success )



handleSyncDetails: String -> TaskDetailsData -> (Model, Cmd TaskDetailsAction, CA.ActionResult)
handleSyncDetails newDetails data =
    let
        taskDetails = data.taskDetails
        newTaskDetails = { taskDetails | details = newDetails }
        newModel = Just { data | dirty = True, taskDetails = newTaskDetails }
    in
       ( newModel, Cmd.none, CA.Success )



handleToggleState: TaskDetailsData -> (Model, Cmd TaskDetailsAction, CA.ActionResult)
handleToggleState data =
    let
        newState = case data.taskDetails.state of
            New -> InProgress
            InProgress -> Done
            Done -> New
        details = data.taskDetails
        newDetails = { details | state = newState }
        newData = { data | dirty = True, taskDetails = newDetails }
    in
        ( Just newData, Cmd.none, CA.Success )



handleTaskUpdating: TaskDetailsData -> (Model, Cmd TaskDetailsAction, CA.ActionResult)
handleTaskUpdating tdd =
    let
        newModel = { tdd | updating = True }
        newCmd   = TR.updateTaskDetails tdd.taskDetails TaskUpdated
    in
        ( Just newModel, newCmd, CA.Success )



handleTaskUpdated: (Result Http.Error ()) -> TaskDetailsData -> (Model, Cmd TaskDetailsAction, CA.ActionResult)
handleTaskUpdated result tdd =
    case result of
        Ok _       -> ( Just { tdd | updating = False, dirty = False }, Cmd.none, CA.SuccessWithMessage "Task updated" )
        Err reason -> ( Just { tdd | updating = False }, Cmd.none, CA.NetworkError <| "Error updating task " ++ (toString reason) )
