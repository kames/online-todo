module Tasks.Details.Module exposing (taskDetailsModule)

import Common.Actions as CA
import Common.Module as CM

import Tasks.Details.View   exposing (..)
import Tasks.Details.Model  exposing (..)
import Tasks.Details.Action exposing (..)


taskDetailsModule: CM.Module TaskDetailsAction Model
taskDetailsModule = CM.Module view update (initialModel, Cmd.none, CA.Success)
