module Tasks.Model exposing (..)

import Tasks.Task exposing (..)
import Tasks.Details.Model as TDM



type alias Model =
    { tasks          : List Task
    , showDone       : Bool
    , tasksLoading   : Bool
    , newTaskSummary : String
    , newTaskAdding  : Bool
    , details        : TDM.Model
    }



initialModel : Model
initialModel = Model [] False True "" False Nothing



emptyModel: Model
emptyModel = Model [] False False "" False Nothing
