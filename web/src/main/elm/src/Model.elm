module Model exposing (..)

import Tasks.Model as TM
import Settings.Model as SM
import Notes.Notes as N



type Mode = ModeTasks
          | ModeNotes
          | ModeSettings



type MessageType = Success
                 | Error
                 | Info



type alias MessageBanner =
    { context: String
    , msgType: MessageType }



type alias Model =
    { mode           : Mode
    , tasksModel     : TM.Model
    , notesModel     : N.NotesModel
    , settingsModel  : Maybe SM.Model
    , message        : Maybe MessageBanner
    }



initialModel : Model
initialModel = Model ModeTasks TM.initialModel N.Empty Nothing Nothing
