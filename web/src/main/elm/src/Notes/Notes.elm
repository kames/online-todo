module Notes.Notes exposing (..)

import Html exposing (Html, div, text)

type NotesModel = Empty


notesHtml: NotesModel -> Html msg
notesHtml _ =
    div [ ] [ text "Notes under construction" ]