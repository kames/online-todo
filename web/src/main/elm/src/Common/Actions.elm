module Common.Actions exposing (..)

import Task



type ActionResult
    = Success
    | SuccessWithMessage String
    | NetworkError String



toCmd: a -> Cmd a
toCmd x =
  Task.succeed x
  |> Task.perform identity