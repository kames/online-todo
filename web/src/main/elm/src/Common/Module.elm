module Common.Module exposing (..)

import Common.Actions as CA
import Html exposing (Html)


type alias Module action model =
    { view: model -> Html action
    , update: action -> model -> (model, Cmd action, CA.ActionResult)
    , initialState: (model, Cmd action, CA.ActionResult)
    }
