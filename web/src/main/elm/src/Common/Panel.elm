module Common.Panel exposing (..)

import Html exposing (..)
panel: String -> List (Html a) -> Html a
panel title content =
    div [] content