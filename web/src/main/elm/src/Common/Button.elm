module Common.Button exposing (dosButton)

import Html exposing (Html, div, text)
import Html.Attributes exposing (class, disabled)
import Html.Events exposing (onClick)

dosButton: String -> msg -> Bool -> Html msg
dosButton label action dis =
    let
        cl = "button-dos" ++ (if dis then " button-disabled" else " clickable")
        attributes = (class cl) :: (disabled dis) :: (if dis then [] else [ onClick action ])
    in
        div attributes [ text <| "<<" ++ label ++ ">>"]
