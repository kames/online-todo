module Common.BorderBox exposing (borderBox)

import Html exposing (Html, div, text)
import Html.Attributes exposing (class)



borderBox: String -> Html a -> Html a
borderBox title content =
    div [ class "border-box-outer" ] [
      div [ class "border-box-inner" ] [
        div [ class "border-box-title" ] [ text title ],
        content
      ]
    ]
