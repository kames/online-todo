module Action exposing (..)

import Model exposing (..)
import Tasks.Action as TA
import Tasks.Model as TM
import Rest.Tasks as TR
import Settings.Action as SA
import Settings.Model  as SM
import Notes.Notes as NN
import Common.Actions as CA



--------------------------------------------------------------------------------
-- action
--------------------------------------------------------------------------------
type Action
    = SwitchToTasks
    | SwitchToNotes
    | SwitchToSettings
    | ForTask TA.Action
    | ForSettings SA.SettingsAction
    | ForNotes
    | ShowMessage MessageType String
    | HideMessage



--------------------------------------------------------------------------------
-- update function
--------------------------------------------------------------------------------
update : Action -> Model -> (Model, Cmd Action)
update action model =
        case action of
            ShowMessage t m    -> handleMessage model m t
            HideMessage        -> hideMessage model
            SwitchToNotes      -> handleSwitchToNotes model
            SwitchToTasks      -> handleSwitchToTasks model
            SwitchToSettings   -> handleSwitchToSettings model
            ForSettings sa     -> handleSettingsAction model sa
            ForNotes           -> ( model, Cmd.none )
            ForTask taskAction -> handleTaskAction model taskAction



--------------------------------------------------------------------------------
-- initial action
--------------------------------------------------------------------------------
initialAction: Cmd Action
initialAction = Cmd.map ForTask TA.initialAction


--------------------------------------------------------------------------------
-- handlers
--------------------------------------------------------------------------------
handleMessage: Model -> String -> MessageType -> (Model, Cmd Action)
handleMessage model message msgType =
    let
        newModel = { model | message = Just <| MessageBanner message msgType }
    in
        ( newModel, Cmd.none )



hideMessage: Model -> (Model, Cmd Action)
hideMessage model = ( { model | message = Nothing }, Cmd.none )



handleSwitchToNotes: Model -> (Model, Cmd Action)
handleSwitchToNotes model =
    let
        newModel = { model | mode = ModeNotes
                           , tasksModel = TM.emptyModel
                           , notesModel = NN.Empty
                           , settingsModel = Nothing }
    in
        ( newModel, Cmd.none )



handleSwitchToTasks: Model -> (Model, Cmd Action)
handleSwitchToTasks model =
    let
        newModel = { model | mode = ModeTasks
                           , tasksModel = TM.initialModel
                           , notesModel = NN.Empty
                           , settingsModel = Nothing }
    in
        ( newModel, Cmd.map ForTask (TR.fetchTasks TA.TasksLoaded) )



handleSwitchToSettings: Model -> (Model, Cmd Action)
handleSwitchToSettings model =
    let
        newModel = { model | mode = ModeSettings
                           , tasksModel = TM.emptyModel
                           , notesModel = NN.Empty
                           , settingsModel = Just SM.initialModel }
    in
        ( newModel, Cmd.none )



handleTaskAction: Model -> TA.Action -> ( Model, Cmd Action )
handleTaskAction model taskAction =
    let
        (taskModel, nextTaskAction, actionResult) = TA.update taskAction model.tasksModel
        newModel = { model | tasksModel = taskModel }
        newCmd = case actionResult of
            CA.Success ->
                Cmd.map ForTask nextTaskAction
            CA.SuccessWithMessage msg ->
                Cmd.batch [Cmd.map ForTask nextTaskAction, Cmd.map (ShowMessage Success) <| CA.toCmd msg]
            CA.NetworkError msg ->
                Cmd.batch [Cmd.map ForTask nextTaskAction, Cmd.map (ShowMessage Error) <| CA.toCmd msg]
    in
        ( newModel, newCmd )



handleSettingsAction: Model -> SA.SettingsAction -> (Model, Cmd Action)
handleSettingsAction =
    let
        submoduleGetter m = m.settingsModel
        submoduleSetter m sm = { m | settingsModel = Just sm }
        subUpdate = SA.update
        actionCreator = ForSettings
    in
        handleSubmoduleAction submoduleGetter submoduleSetter subUpdate actionCreator



handleSubmoduleAction:
    (Model -> Maybe sm) ->
    (Model -> sm -> Model) ->
    (sa -> sm -> (sm, Cmd sa, CA.ActionResult)) ->
    (sa -> Action) ->
    Model -> sa -> (Model, Cmd Action)
handleSubmoduleAction submodelGetter submodelSetter subUpdate actionCreator model action =
    case (submodelGetter model) of
        Nothing       -> ( model, Cmd.none )
        Just submodel ->
            let
                ( newSubModel, nextSubAction, actionResult ) = subUpdate action submodel
                newModel = submodelSetter model newSubModel
                newCmd = case actionResult of
                    CA.Success ->
                        Cmd.map actionCreator nextSubAction
                    CA.SuccessWithMessage msg ->
                        Cmd.batch [Cmd.map actionCreator nextSubAction, Cmd.map (ShowMessage Success) <| CA.toCmd msg]
                    CA.NetworkError msg ->
                        Cmd.batch [Cmd.map actionCreator nextSubAction, Cmd.map (ShowMessage Error) <| CA.toCmd msg]
            in
                ( newModel, newCmd )
