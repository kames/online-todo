DROP TABLE IF EXISTS tasks;
DROP TABLE IF EXISTS users;



CREATE TABLE users (
   id              VARCHAR(40) NOT NULL UNIQUE,
   name            VARCHAR(100) NOT NULL,
   email           VARCHAR(100) NOT NULL,
   primary key (id)
);



CREATE TABLE tasks (
   id              VARCHAR(40) NOT NULL UNIQUE,
   userId          VARCHAR(40) NOT NULL REFERENCES users(id),
   summary         VARCHAR(250) NOT NULL,
   details         TEXT NOT NULL,
   state           VARCHAR(15) NOT NULL,
   creationDate    TIMESTAMP NOT NULL,
   primary key (id)
);
