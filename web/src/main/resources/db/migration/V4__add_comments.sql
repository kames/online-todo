CREATE TABLE comments (
   id              VARCHAR(40) NOT NULL UNIQUE,
   userId          VARCHAR(40) NOT NULL REFERENCES users(id),
   taskId          VARCHAR(40) NOT NULL REFERENCES tasks(id),
   content         TEXT NOT NULL,
   creationDate    TIMESTAMP NOT NULL,
   primary key (id)
);
