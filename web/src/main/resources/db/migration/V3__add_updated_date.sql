ALTER TABLE tasks ADD updatedDate TIMESTAMP NOT NULL DEFAULT NOW();
UPDATE tasks SET updatedDate = creationDate;