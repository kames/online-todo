////////////////////////////////////////////////////////////////////////////////
// @Copyright Magic Turtle Software, 2019
////////////////////////////////////////////////////////////////////////////////
package io.magicturtle.onlinetodos

import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.core.annotation.Order
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter


/**
 * @author Marek Broda (@Magic Turtle Software)
 */
@Configuration
@Order(-100)
@Profile("local")
class LocalSecurityConfiguration  : WebSecurityConfigurerAdapter() {

    override fun configure(http: HttpSecurity?) {
        // no security for you
        http!!.csrf().disable()
    }
}