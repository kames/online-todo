////////////////////////////////////////////////////////////////////////////////
// @Copyright Magic Turtle Software, 2019
////////////////////////////////////////////////////////////////////////////////
package io.magicturtle.onlinetodos.dao

import io.magicturtle.onlinetodos.model.Comment
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.RowMapper
import org.springframework.stereotype.Repository
import java.sql.ResultSet

/**
 * @author Marek Broda (@Magic Turtle Software)
 */
@Repository
class CommentRepository( val db: JdbcTemplate ) {

    fun getCommentsForTask(taskId: String, userId: String): List<Comment> {
        with (Comment) {
            val query = """
                SELECT $fields
                FROM $tableName
                WHERE ${Comment.Sql.Columns.taskId} = ? AND ${Comment.Sql.Columns.userId} = ?
                ORDER BY ${Comment.Sql.Columns.creationDate} DESC
            """.trimIndent()

            return db.query(query, arrayOf(taskId, userId), commentMapper)
        }
    }



    fun createComment(comment: Comment) {
        with (Comment) {
            val query = "INSERT INTO $tableName ($fields) VALUES (?, ?, ?, ?, ?)"
            val params = params(comment)

            db.batchUpdate(query, listOf(params))
        }
    }



    val commentMapper: RowMapper<Comment> =
        RowMapper { rs: ResultSet, _: Int ->
            with (Comment.Sql.Columns) {
                Comment(
                    rs.getString(id),
                    rs.getString(taskId),
                    rs.getString(userId),
                    rs.getTimestamp(creationDate).toLocalDateTime(),
                    rs.getString(content))
            }
        }
}