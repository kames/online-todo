////////////////////////////////////////////////////////////////////////////////
// Copyright @ Magic Turtle Software, 2019                                    //
////////////////////////////////////////////////////////////////////////////////
package io.magicturtle.onlinetodos.dao

import io.magicturtle.onlinetodos.model.User
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.RowMapper
import org.springframework.stereotype.Repository
import java.sql.ResultSet



/**
 * @author Marek Broda (@Magic Turtle Software)
 */
@Repository
class UserRepository( val db: JdbcTemplate ) {

    fun getUser(id: String): User? {
        with (User) {
            val results = db.query(
                """SELECT $columnId, $columnName, $columnEmail, $columnPass
                   FROM $tableName
                   WHERE $columnId = ?""",
                userMapper, id
            )

            return if (results.size == 1) results[0] else null
        }
    }



    fun getUserByEmail(email: String): User? {
        with (User) {
            val results = db.query(
                """SELECT $columnId, $columnName, $columnEmail, $columnPass
                   FROM $tableName
                   WHERE $columnEmail = ?""",
                userMapper, email
            )

            return if (results.size == 1) results[0] else null
        }
    }



    fun createUser(userId: String, username: String, email: String): User {
        with (User) {
            db.batchUpdate(
                "INSERT INTO $tableName($columnId, $columnName, $columnEmail) VALUES (?, ?, ?)",
                listOf(arrayOf(userId, username, email)))

            return User(userId, username, email, null)
        }
    }



    fun updateUser(user: User) {
        with (User) {
            db.batchUpdate(
                """UPDATE $tableName
                   SET $columnName = ?,
                       $columnEmail = ?,
                       $columnPass = ?
                   WHERE $columnId = ?""",
                listOf(arrayOf(user.name, user.email, user.password, user.id)))
        }
    }



    companion object {
        val userMapper: RowMapper<User> =
            with (User) {
                RowMapper { rs: ResultSet, _: Int ->
                    User(rs.getString(columnId),
                         rs.getString(columnName),
                         rs.getString(columnEmail),
                         rs.getString(columnPass)) }
            }
    }
}