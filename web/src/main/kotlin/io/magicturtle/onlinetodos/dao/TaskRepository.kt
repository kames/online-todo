////////////////////////////////////////////////////////////////////////////////
// Copyright @ Magic Turtle Software, 2019                                     /
////////////////////////////////////////////////////////////////////////////////
package io.magicturtle.onlinetodos.dao

import io.magicturtle.onlinetodos.model.Task
import io.magicturtle.onlinetodos.model.TaskStatus
import io.magicturtle.onlinetodos.model.User
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.RowMapper
import org.springframework.stereotype.Repository
import java.sql.ResultSet


/**
 * @author Marek Broda (@Magic Turtle Software)
 */
@Repository
class TaskRepository ( val db: JdbcTemplate ) {

    fun getTask(id: String, currentUser: User): Task? {
        with( Task ) {
            val query = """
                SELECT $columnId, $columnUserId, $columnSummary, $columnDetails, $columnState, $columnCreationDate, $columnUpdatedDate
                FROM $tableName
                WHERE $columnId = ? AND $columnUserId = ?"""

            val results = db.query(query, arrayOf(id, currentUser.id), taskMapper)
            return if (results.size == 1) results[0] else null
        }
    }



    fun getAllTasks(showDone: Boolean? = null, currentUser: User): List<Task> {
        with( Task ) {
            val statusConstraint = when (showDone) {
                null  -> ""
                true  -> "AND $columnState = ?"
                false -> "AND $columnState <> ?"
            }

            val query = """
               SELECT $columnId, $columnUserId, $columnSummary, $columnDetails, $columnState, $columnCreationDate, $columnUpdatedDate
               FROM $tableName
               WHERE $columnUserId = ? $statusConstraint"""

            val params = if (statusConstraint.length > 0)
                             arrayOf(currentUser.id, TaskStatus.Done.name)
                         else
                             arrayOf(currentUser.id)

            return db.query(query, params, taskMapper)
        }
    }



    fun createTask(task: Task) {
        with( Task ) {
            val query = """
                INSERT INTO $tableName($columnId, $columnUserId, $columnSummary, $columnDetails, $columnState, $columnCreationDate, $columnUpdatedDate)
                VALUES (?, ?, ?, ?, ?, ?, ?)"""
            val params = arrayOf(task.id, task.userId, task.summary, task.details, task.state.name, task.creationDate, task.updatedDate)

            db.batchUpdate(query, listOf(params))
        }
    }



    fun removeAllTasks() {
        db.execute("DELETE FROM ${Task.tableName}")
    }



    fun updateTask(task: Task, currentUser: User) {
        with( Task ) {
            val query = """
                UPDATE $tableName
                SET $columnSummary = ?, $columnDetails = ?, $columnState = ?
                WHERE $columnId = ? AND $columnUserId = ? AND $columnUpdatedDate = ?"""
            val params = arrayOf(task.summary, task.details, task.state.name, task.id, task.userId, task.updatedDate)

            db.batchUpdate(query, listOf(params))
        }
    }


    val taskMapper: RowMapper<Task> =
        RowMapper { rs: ResultSet, _: Int ->
            Task(rs.getString("id"),
                 rs.getString("userId"),
                 rs.getString("summary"),
                 rs.getString("details"),
                 TaskStatus.valueOf(rs.getString("state")),
                 rs.getTimestamp("creationDate").toLocalDateTime(),
                 rs.getTimestamp("updatedDate").toLocalDateTime())}
}