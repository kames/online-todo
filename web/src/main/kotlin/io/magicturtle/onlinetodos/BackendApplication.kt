////////////////////////////////////////////////////////////////////////////////
// Copyright @ Magic Turtle Software, 2019                                     /
////////////////////////////////////////////////////////////////////////////////
package io.magicturtle.onlinetodos

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.flyway.FlywayMigrationStrategy
import org.springframework.boot.jdbc.DataSourceBuilder
import org.springframework.context.annotation.Bean
import org.springframework.core.env.ConfigurableEnvironment
import org.springframework.util.StringUtils
import java.net.URI
import java.net.URISyntaxException
import javax.sql.DataSource


/**
 * @author Marek Broda (@Magic Turtle Software)
 */
@SpringBootApplication
class BackendApplication {

    @Autowired
    var env: ConfigurableEnvironment? = null



    @Bean
    fun createFlywayConfigurationBean(): FlywayMigrationStrategy {
        return FlywayMigrationStrategy { flyway -> flyway!!.migrate() }
    }



    @Bean
    fun createDataSource(): DataSource {
        val driverClass = getProperty("spring.datasource.driver-class-name")
        val databaseUrl = getProperty("database.url")

        if (databaseUrl == "h2") {
            return DataSourceBuilder.create()
                .driverClassName(driverClass)
                .url("jdbc:h2:mem:test;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE")
                .build()
        } else {
            val uri = URI(databaseUrl)

            val username = uri.userInfo.split(":")[0]
            val password = uri.userInfo.split(":")[1]
            val dbUrl = "jdbc:postgresql://" + uri.host + ':' + uri.port + uri.path

            return DataSourceBuilder.create()
                .driverClassName("org.postgresql.Driver")
                .password(password)
                .username(username)
                .url(dbUrl)
                .build()
        }
    }



    private fun getProperty(propKey: String): String {
        val envKey = propKey.toUpperCase().replace('.', '_')

        val envVar = System.getenv(envKey)
        val propVar = env!!.getProperty(propKey)

        if (!StringUtils.isEmpty(envVar)) {
            return envVar
        } else if (!StringUtils.isEmpty(propVar)) {
            return propVar!!
        } else {
            return ""
        }
    }
}



fun main() {
    SpringApplication.run(BackendApplication::class.java)
}
