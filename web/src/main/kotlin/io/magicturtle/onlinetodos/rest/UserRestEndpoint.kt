////////////////////////////////////////////////////////////////////////////////
// @Copyright Magic Turtle Software, 2019
////////////////////////////////////////////////////////////////////////////////
package io.magicturtle.onlinetodos.rest

import io.magicturtle.onlinetodos.service.UserService
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController



/**
 * @author Marek Broda (@Magic Turtle Software)
 */
@RestController
class UserRestEndpoint( val userService: UserService ) {

    @PostMapping("/api/settings/updatePassword")
    fun updatePassword(@RequestBody newPassword: String) {
        val user = userService.getCurrentUser()
        userService.updatePassword(newPassword, user)
    }
}