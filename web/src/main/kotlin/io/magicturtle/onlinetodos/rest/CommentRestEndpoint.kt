////////////////////////////////////////////////////////////////////////////////
// @Copyright Magic Turtle Software, 2019
////////////////////////////////////////////////////////////////////////////////
package io.magicturtle.onlinetodos.rest

import io.magicturtle.onlinetodos.model.Comment
import io.magicturtle.onlinetodos.service.CommentService
import io.magicturtle.onlinetodos.service.TaskService
import io.magicturtle.onlinetodos.service.UserService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController



/**
 * @author Marek Broda (@Magic Turtle Software)
 */
@RestController
class CommentRestEndpoint( val userService: UserService
                         , val commentService: CommentService
                         , val taskService: TaskService) {

    @PostMapping("/api/task/{taskId}/comment")
    fun createComment(@PathVariable taskId: String, @RequestBody content: String): ResponseEntity<Comment> {
        val user = userService.getCurrentUser()

        if (!taskService.doesTaskExist(taskId, user)) {
            return ResponseEntity(null as Comment?, HttpStatus.NOT_FOUND)
        }

        return commentService.createComment(taskId, user, content)
                             .map       { ResponseEntity(it, HttpStatus.OK) }
                             .otherwise { ResponseEntity(null as Comment?, it.code) }
    }
}
