package io.magicturtle.onlinetodos.rest

import io.magicturtle.onlinetodos.dto.TaskDetailsDTO
import io.magicturtle.onlinetodos.dto.UpdateTaskDTO
import io.magicturtle.onlinetodos.model.Task
import io.magicturtle.onlinetodos.service.TaskService
import io.magicturtle.onlinetodos.service.TaskService.TasksMode
import io.magicturtle.onlinetodos.service.UserService
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*


/**
 * @author Marek Broda (@Magic Turtle Software)
 */
@RestController
class TaskRestEndpoint ( val taskService: TaskService
                       , val userService: UserService
                       ) {

    @GetMapping("/api/tasks")
    fun getAllTodoItems(@RequestParam("mode", defaultValue = "NOT_DONE") mode: TasksMode): List<Task> {
        val currentUser = userService.getCurrentUser()
        return taskService.getTasks(mode, currentUser)
    }



    @PostMapping("/api/task")
    fun addTask(@RequestBody summary: String): ResponseEntity<Task> {
        val currentUser = userService.getCurrentUser()
        val result = taskService.createTask(summary, currentUser)

        return result.map       { ResponseEntity(it,   HttpStatus.OK) }
                     .getOrElse { ResponseEntity(null as Task?, HttpStatus.BAD_REQUEST) }
    }



    @PostMapping(path = arrayOf("/api/task/{id}"), consumes = arrayOf(MediaType.APPLICATION_JSON_VALUE))
    fun updateTask(@PathVariable("id") id: String, @RequestBody taskDetails: UpdateTaskDTO): ResponseEntity<Task> {
        val currentUser = userService.getCurrentUser()
        return taskService.updateTask(id, taskDetails, currentUser)
                          .map       { ResponseEntity( it, HttpStatus.OK ) }
                          .otherwise { ResponseEntity( null as Task?, it.code ) }
    }



    @PostMapping("/api/task/{id}/cycle")
    fun cycleTask(@PathVariable("id") id: String): Task {
        val currentUser = userService.getCurrentUser()
        return taskService.cycleTask(id, currentUser)
    }



    @GetMapping("/api/task/{id}")
    fun getTask(@PathVariable("id") id: String): ResponseEntity<TaskDetailsDTO> {
        val currentUser = userService.getCurrentUser()

        return taskService.getTaskDetails(id, currentUser)
                          .map       { ResponseEntity(it, HttpStatus.OK) }
                          .otherwise { ResponseEntity(null as TaskDetailsDTO?, it.code) }
    }
}