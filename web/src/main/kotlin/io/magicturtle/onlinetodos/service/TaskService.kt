////////////////////////////////////////////////////////////////////////////////
// @Copyright Magic Turtle Software, 2019
////////////////////////////////////////////////////////////////////////////////
package io.magicturtle.onlinetodos.service

import io.magicturtle.onlinetodos.dao.CommentRepository
import io.magicturtle.onlinetodos.dao.TaskRepository
import io.magicturtle.onlinetodos.dto.TaskDetailsDTO
import io.magicturtle.onlinetodos.dto.UpdateTaskDTO
import io.magicturtle.onlinetodos.model.Task
import io.magicturtle.onlinetodos.model.TaskStatus
import io.magicturtle.onlinetodos.model.User
import io.magicturtle.onlinetodos.model.ValidationResult.Error
import io.magicturtle.onlinetodos.model.ValidationResult.Ok
import io.magicturtle.onlinetodos.util.Either
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.data.rest.webmvc.ResourceNotFoundException
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import java.time.LocalDateTime


/**
 * @author Marek Broda (@Magic Turtle Software)
 */
@Service
class TaskService( val taskRepository: TaskRepository
                 , val commentRepository: CommentRepository ) {

    enum class TasksMode {
        ALL, DONE, NOT_DONE;
    }



    fun createTask(summary: String, currentUser: User): Either<String, Task> {
        val task = Task(currentUser.id, summary, "", TaskStatus.New)

        try {
            taskRepository.createTask(task)
            return Either.Value(task)
        } catch (e: DataIntegrityViolationException) {
            return Either.Error("Can't create task with too long summary")
        }
    }



    fun getTasks(tasksMode: TasksMode, currentUser: User): List<Task> {
        val showDone = when (tasksMode) {
            TasksMode.ALL -> null
            TasksMode.DONE -> true
            TasksMode.NOT_DONE -> false
        }

        return taskRepository.getAllTasks(showDone, currentUser)
                             .sortedWith(compareBy { it.creationDate })
                             .asReversed()
    }



    fun updateTask(id: String, taskDetails: UpdateTaskDTO, currentUser: User): ServiceResult<Task> {
        val task = taskRepository.getTask(id, currentUser)
                ?: return Either.Error(ErrorReason(HttpStatus.NOT_FOUND, "No task with given id: $id"))

        val updatedTask = task.copy(
            summary     = taskDetails.summary,
            details     = taskDetails.details,
            state       = taskDetails.state,
            updatedDate = LocalDateTime.now())


        when (val result = updatedTask.validate()) {
            is Ok -> {
                taskRepository.updateTask(updatedTask, currentUser)
                return Either.Value(updatedTask)
            }
            is Error -> {
                return Either.Error(ErrorReason(HttpStatus.BAD_REQUEST, result.msg))
            }
        }
    }



    fun cycleTask(taskId: String, currentUser: User): Task {
        val task = taskRepository.getTask(taskId, currentUser)
                ?: throw ResourceNotFoundException("No task with id $taskId")

        val updatedTask = task.copy(state = task.state.cycle())
        taskRepository.updateTask(updatedTask, currentUser)
        return updatedTask
    }



    fun getTaskDetails(taskId: String, currentUser: User): ServiceResult<TaskDetailsDTO> {
        val task = taskRepository.getTask(taskId, currentUser)
                ?: return Either.Error(ErrorReason(HttpStatus.NOT_FOUND, "Can't find task with given id"))

        val comments = commentRepository.getCommentsForTask(taskId, currentUser.id)

        return Either.Value(TaskDetailsDTO(task.id, task.summary, task.details, task.state, comments))
    }



    fun doesTaskExist(taskId: String, currentUser: User): Boolean {
        return taskRepository.getTask(taskId, currentUser) != null
    }
}

data class ErrorReason( val code: HttpStatus, val summary: String)
typealias ServiceResult<T> = Either<ErrorReason, T>
