////////////////////////////////////////////////////////////////////////////////
// @Copyright Magic Turtle Software, 2019
////////////////////////////////////////////////////////////////////////////////
package io.magicturtle.onlinetodos.service

import io.magicturtle.onlinetodos.dao.CommentRepository
import io.magicturtle.onlinetodos.model.Comment
import io.magicturtle.onlinetodos.model.User
import io.magicturtle.onlinetodos.model.ValidationResult
import io.magicturtle.onlinetodos.util.Either
import io.magicturtle.onlinetodos.util.Either.Error
import org.springframework.http.HttpStatus.BAD_REQUEST
import org.springframework.stereotype.Service


/**
 * @author Marek Broda (@Magic Turtle Software)
 */
@Service
class CommentService( val commentRepository: CommentRepository) {

    fun createComment(taskId: String, user: User, content: String): ServiceResult<Comment> {
        val comment = Comment(taskId, user.id, content)

        when (val r = comment.validate()) {
            is ValidationResult.Error -> return Error(ErrorReason(BAD_REQUEST, r.msg))
        }

        commentRepository.createComment(comment)
        return Either.Value(comment)
    }
}
