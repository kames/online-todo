////////////////////////////////////////////////////////////////////////////////
// @Copyright Magic Turtle Software, 2019
////////////////////////////////////////////////////////////////////////////////
package io.magicturtle.onlinetodos.service

import io.magicturtle.onlinetodos.dao.UserRepository
import io.magicturtle.onlinetodos.model.User
import org.springframework.http.HttpStatus
import org.springframework.security.authentication.AnonymousAuthenticationToken
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.oauth2.provider.OAuth2Authentication
import org.springframework.stereotype.Service
import org.springframework.web.bind.annotation.ResponseStatus
import java.util.*


/**
 * @author Marek Broda (@Magic Turtle Software)
 */
@Service
class UserService( val userRepository: UserRepository
                 ) {

    fun updatePassword(password: String, currentUser: User): User {
        val saltedPassword = PASSWORD_SALT + password
        val passwordHash = BCryptPasswordEncoder().encode(saltedPassword)
        val newUser = currentUser.copy(password = passwordHash)
        userRepository.updateUser(newUser)
        return newUser
    }



    fun getCurrentUser(): User {
        val authentication = SecurityContextHolder.getContext().authentication

        return when (authentication) {
            is AnonymousAuthenticationToken        ->
                getOrCreateDefaultUser()
            is OAuth2Authentication                ->
                getOrCreateOAuthUser(authentication)
            is UsernamePasswordAuthenticationToken ->
                userRepository.getUserByEmail(authentication.principal as String)!!
            else                                   ->
                throw RuntimeException("Unknown type of authentication: ${authentication::class.simpleName}")
        }
    }



    private fun getOrCreateOAuthUser(authentication: OAuth2Authentication): User {
        val userDetails = authentication.userAuthentication.details as Map<*, *>

        val userId   = userDetails["id"]    as String
        val username = userDetails["name"]  as String
        val email    = userDetails["email"] as String

        return userRepository.getUser(userId)
            ?: userRepository.createUser(userId, username, email)
    }



    fun getOrCreateDefaultUser(): User {
        return userRepository.getUser(DEFAULT_USER_ID)
            ?: userRepository.createUser(DEFAULT_USER_ID, DEFAULT_USER_NAME, DEFAULT_USER_EMAIL)
    }



    fun getOrCreateUser(email: String, name: String): User {
        return userRepository.getUserByEmail(email)
            ?: userRepository.createUser(UUID.randomUUID().toString(), name, email)
    }



    fun areCredentialsCorrect(username: String, password: String): Boolean {
        val user = userRepository.getUserByEmail(username) ?: return false
        return BCryptPasswordEncoder().matches(PASSWORD_SALT + password, user.password)
    }


    @ResponseStatus( value = HttpStatus.UNAUTHORIZED
        , reason = "No authenticated user present")
    class NoAuthenticatedUser(message: String) : RuntimeException(message)



    companion object {
        const val PASSWORD_SALT = "i_pysznie_posolone_"
        const val DEFAULT_USER_ID = "razdwatrzy"
        const val DEFAULT_USER_NAME = "Anon Nymous"
        const val DEFAULT_USER_EMAIL = "anon@nymous.it"
    }
}



