////////////////////////////////////////////////////////////////////////////////
// @Copyright Magic Turtle Software, 2019
////////////////////////////////////////////////////////////////////////////////
package io.magicturtle.onlinetodos.util

/**
 * @author Marek Broda (@Magic Turtle Software)
 */
sealed class Either<out A, B> {

    data class Error<A,B>(val value: A) : Either<A,B>()
    data class Value<A,B>(val value: B) : Either<A,B>()



    inline infix fun <C> map(mapper: (B) -> C): Either<A, C> {
        return when (this) {
            is Error -> Error(value)
            is Value -> Value(mapper.invoke(value))
        }
    }



    inline infix fun getOrElse(p: () -> B): B {
        return when (this) {
            is Error -> p.invoke()
            is Value -> value
        }
    }



    inline infix fun otherwise(p: (A) -> B): B {
        return when (this) {
            is Error -> p.invoke(value)
            is Value -> value
        }
    }



    fun force(): B {
        return when (this) {
            is Value -> value
            is Error -> throw RuntimeException("This is an error, can't fetch value!")
        }
    }
}