////////////////////////////////////////////////////////////////////////////////
// @Copyright Magic Turtle Software, 2019
////////////////////////////////////////////////////////////////////////////////
package io.magicturtle.onlinetodos.dto

import io.magicturtle.onlinetodos.model.TaskStatus


/**
 * @author Marek Broda (@Magic Turtle Software)
 */
data class UpdateTaskDTO( val summary: String
                        , val details: String
                        , val state: TaskStatus )