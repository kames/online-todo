package io.magicturtle.onlinetodos.dto

import io.magicturtle.onlinetodos.model.Comment
import io.magicturtle.onlinetodos.model.TaskStatus


/**
 * @author Marek Broda (@Iteratec)
 */
data class TaskDetailsDTO ( val id: String
                          , val summary: String
                          , val details: String
                          , val state: TaskStatus
                          , val comments: List<Comment> )