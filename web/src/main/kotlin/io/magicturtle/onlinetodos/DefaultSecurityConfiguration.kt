////////////////////////////////////////////////////////////////////////////////
// @Copyright Magic Turtle Software, 2019
////////////////////////////////////////////////////////////////////////////////
package io.magicturtle.onlinetodos

import io.magicturtle.onlinetodos.service.UserService
import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.core.annotation.Order
import org.springframework.security.authentication.AuthenticationProvider
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.core.Authentication
import java.util.*


/**
 * @author Marek Broda (@Magic Turtle Software)
 */
@Configuration
@EnableWebSecurity
@EnableOAuth2Sso
@Order(-100)
@Profile("default")
class DefaultSecurityConfiguration( val userService: UserService ) : WebSecurityConfigurerAdapter() {
    inner class UserAuthentication : AuthenticationProvider {
        override fun authenticate(authentication: Authentication?): Authentication {
            val username = authentication!!.name
            val password = authentication.credentials.toString()

            if (userService.areCredentialsCorrect(username, password)) {
                return UsernamePasswordAuthenticationToken(username, password, Collections.emptyList())
            } else {
                throw BadCredentialsException("Bad email or password")
            }
        }


        override fun supports(authentication: Class<*>?): Boolean {
            return true
        }

    }

    override fun configure(http: HttpSecurity?) {
        http!!
            .authorizeRequests()
                .antMatchers("/api/login**").permitAll()
                .anyRequest().authenticated()
                .and()
            .csrf()
                .disable()
            .httpBasic()
                .and()
            .authenticationProvider(UserAuthentication())
    }
}