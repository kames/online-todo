////////////////////////////////////////////////////////////////////////////////
// Copyright @ Magic Turtle Software, 2019                                     /
////////////////////////////////////////////////////////////////////////////////
package io.magicturtle.onlinetodos.model



/**
 * @author Marek Broda
 */
data class TaskNoteAssociation( val id: String
                              , val note: Note
                              , val task: Task)