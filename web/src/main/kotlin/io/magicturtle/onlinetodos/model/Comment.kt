////////////////////////////////////////////////////////////////////////////////
// @Copyright Magic Turtle Software, 2019
////////////////////////////////////////////////////////////////////////////////
package io.magicturtle.onlinetodos.model

import io.magicturtle.onlinetodos.model.ValidationResult.Error
import io.magicturtle.onlinetodos.model.ValidationResult.Ok
import java.time.LocalDateTime
import java.util.*



/**
 * @author Marek Broda (@Magic Turtle Software)
 */
data class Comment( val id: String
                  , val taskId: String
                  , val userId: String
                  , val creationDate: LocalDateTime
                  , val content: String ) {

    constructor(taskId: String, userId: String, content: String)
        : this(UUID.randomUUID().toString(), taskId, userId, LocalDateTime.now(), content)



    fun validate(): ValidationResult {
        if (content.isBlank()) {
            return Error("Comment has no content")
        }

        return Ok()
    }



    companion object Sql {
        const val tableName = "comments"

        val fields = with (Columns) { "$id, $taskId, $userId, $creationDate, $content" }
        fun params(c: Comment) = arrayOf(c.id, c.taskId, c.userId, c.creationDate, c.content)

        object Columns {
            const val id           = "id"
            const val taskId       = "taskId"
            const val userId       = "userId"
            const val creationDate = "creationDate"
            const val content      = "content"
        }
    }
}