////////////////////////////////////////////////////////////////////////////////
// Copyright @ Magic Turtle Software, 2019                                     /
////////////////////////////////////////////////////////////////////////////////
package io.magicturtle.onlinetodos.model



/**
 * @author Marek Broda (@Magic Turtle Software)
 */
data class User ( val id:       String
                , val name:     String
                , val email:    String
                , val password: String?) {



    companion object {
        const val tableName   = "users"
        const val columnId    = "id"
        const val columnName  = "name"
        const val columnEmail = "email"
        const val columnPass  = "password"
    }
}