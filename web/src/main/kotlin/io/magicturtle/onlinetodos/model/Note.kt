////////////////////////////////////////////////////////////////////////////////
// Copyright @ Magic Turtle Software, 2019                                     /
////////////////////////////////////////////////////////////////////////////////
package io.magicturtle.onlinetodos.model



data class Note ( val id: String
                , val name: String
                , val content: String
                , val colour: Colour = Colour.CYAN)



enum class Colour {
    CYAN, MAGENTA, BLACK, TEAL
}