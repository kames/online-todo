////////////////////////////////////////////////////////////////////////////////
// Copyright @ Magic Turtle Software, 2019                                     /
////////////////////////////////////////////////////////////////////////////////
package io.magicturtle.onlinetodos.model

/**
 * @author Marek Broda (@Magic Turtle Software)
 */
sealed class ValidationResult {
    class Ok : ValidationResult()
    class Error(val msg: String) : ValidationResult()
}