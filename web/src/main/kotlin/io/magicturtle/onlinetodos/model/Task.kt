////////////////////////////////////////////////////////////////////////////////
// Copyright @ Magic Turtle Software, 2019                                     /
////////////////////////////////////////////////////////////////////////////////
package io.magicturtle.onlinetodos.model

import io.magicturtle.onlinetodos.model.ValidationResult.Ok
import io.magicturtle.onlinetodos.model.ValidationResult.Error
import java.time.LocalDateTime
import java.util.*



/**
 * @author Marek Broda
 */
data class Task ( val id: String
                , val userId: String
                , val summary: String
                , val details: String
                , val state: TaskStatus
                , val creationDate: LocalDateTime
                , val updatedDate: LocalDateTime) {

    constructor(userId: String, summary: String, details: String, state: TaskStatus)
            : this(UUID.randomUUID().toString(), userId, summary, details, state, LocalDateTime.now(), LocalDateTime.now())



    fun validate(): ValidationResult {
        if (userId == "") {
            return Error("userId may not be empty")
        }

        if (summary == "") {
            return Error("summary may not be empty")
        }

        if (summary.length > 250) {
            return Error("summary must be max 250 characters long")
        }

        return Ok()
    }


    companion object {
        const val tableName          = "tasks"
        const val columnId           = "id"
        const val columnUserId       = "userId"
        const val columnSummary      = "summary"
        const val columnDetails      = "details"
        const val columnState        = "state"
        const val columnCreationDate = "creationDate"
        const val columnUpdatedDate  = "updatedDate"

        val emptyInstance = Task("", "", "", TaskStatus.New)
    }
}


enum class TaskStatus {
    New, InProgress, Done;

    fun cycle(): TaskStatus {
        return when (this) {
            New -> InProgress
            InProgress -> Done
            Done -> New
        }
    }
}