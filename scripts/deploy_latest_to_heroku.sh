#!/usr/bin/env bash

docker tag magicturtlesoftware/slowtasks:latest registry.heroku.com/slowtasks/web \
  && docker push registry.heroku.com/slowtasks/web \
  && heroku container:release web
